//1 (Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування)
//Екранування - це процес додавання спеціальних символів перед символами в тексті, щоб зробити їх безпечними для використання в коді програм.

//2 (Які засоби оголошення функцій ви знаєте?)
//function, функціональний вираз, arrow function, constructor functions, методи об'єктів

//3(Що таке hoisting, як він працює для змінних та функцій?)
// вони піднімаються вгору по своїй області видимості перед виконанням коду


let firstName = prompt("Ваше ім'я");
let lastName = prompt("Ваше прізвище");

function createNewUser(name, last) {
  let user = {
    name: name,
    last: last,
    getLogin: function () {
      return this.name.charAt(0).toLowerCase() + this.last.toLowerCase();
    },
    setBirthday: function () {
      let validDate = false;
      while (!validDate) {
        let answer = prompt('Введіть дату народження (у форматі dd.mm.yyyy): ');
        let dateRegex = /^(\d{2})\.(\d{2})\.(\d{4})$/;
        if (dateRegex.test(answer)) {
          let parts = answer.split('.');
          let day = parseInt(parts[0]);
          let month = parseInt(parts[1]) - 1;
          let year = parseInt(parts[2]);
          let birthday = new Date(year, month, day);
          if (!isNaN(birthday.getTime())) {
            this.birthday = birthday;
            validDate = true;
          }
        }
        if (!validDate) {
          alert('Невірний формат дати. Спробуйте ще раз.');
        }
      }
    },
    getUserBirthday: function () {
      let birthday = this.birthday;
      let day = birthday.getDate();
      let month = birthday.getMonth() + 1;
      let year = birthday.getFullYear();
      return `${day}.${month}.${year}`;
    },
    getPassword: function () {
      let firstLetter = this.name.charAt(0).toUpperCase();
      let lastLower = this.last.toLowerCase();
      let year = this.birthday.getFullYear();
      return `${firstLetter}${lastLower}${year}`;
    }
  };

  user.setBirthday();

  return user;
}

let newUser = createNewUser(firstName, lastName);
console.log(newUser.getUserBirthday());
console.log(newUser.name, newUser.last);
console.log(newUser.getLogin());
console.log(newUser.getPassword());